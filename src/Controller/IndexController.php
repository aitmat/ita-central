<?php

namespace App\Controller;

use App\Entity\Client;
use App\Model\Client\ClientHandler;
use App\Repository\ClientRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/ping", name="app_index")
     */
    public function pingAction()
    {
        return new JsonResponse(['result' => 'pong']);
    }


    /**
     * @Route("/client/encode/password", name="app_encode_password")
     * @param Request $request
     * @param ClientHandler $clientHandler
     * @return JsonResponse
     */
    public function encodePasswordAction(Request $request, ClientHandler $clientHandler)
    {
        $password = $clientHandler->encodePlainPassword($request->query->get('plainPassword'));
        return new JsonResponse([
            'result' => $password
        ]);
    }

    /**
     * @Route("/client/add_social_network/bind", name="app_add_social_network_bind")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param Request $request
     * @param ObjectManager $manager
     * @return JsonResponse
     */
    public function addSocialNetworkActionForBind(ClientRepository $clientRepository, Request $request, ObjectManager $manager)
    {
        $email = $request->request->get('email');
        $vk = $request->request->get('vkId') ?? null;
        $facebook = $request->request->get('faceBookId') ?? null;
        $gmail = $request->request->get('googleId') ?? null;

        /** @var Client $client */
        $client = $clientRepository->findOneByEmail($email);

        $client->setVkId($vk);
        $client->setFaceBookId($facebook);
        $client->setGoogleId($gmail);

        $manager->persist($client);
        $manager->flush();
        return new JsonResponse(['result' => 'ok']);
    }


    /**
     * @Route("/client/passport/{passport}/{email}", name="app_client_exists")
     * @Method("HEAD")
     * @param string $passport
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientExistsAction(string $passport, string $email, ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPassportOrEmail($passport, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/client/check/already_attached/{uid}", name="app_client_exists_already_attached")
     * @Method("HEAD")
     * @param string $uid
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientExistsAlreadyAttachedAction(string $uid, ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneBySocialNetworkUid($uid)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }


    /**
     * @Route("/check_client_credentials/{encodedPassword}/{email}", name="app_check_client_credentials")
     * @Method("HEAD")
     * @param string $email
     * @param string $encodedPassword
     * @param ClientRepository $clientRepository
     * @return JsonResponse || null
     */
    public function checkClientCredentialsAction(
        string $encodedPassword,
        string $email,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findByEmailAndPassword($email, $encodedPassword)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/client/auth_social/if_exists", name="app_auth_client_social_network")
     * @Method("GET")
     * @param ClientRepository $clientRepository
     * @param Request $request
     * @return JsonResponse
     */
    public function checkClientSocialNetworkActionForAuth(ClientRepository $clientRepository, Request $request)
    {
        $uid = $request->query->get('uid');
        /** @var Client $data_user */
        $data_user = $clientRepository->findOneBySocialNetworkUid($uid);
        if ($data_user) {
            return new JsonResponse($data_user->toArray());
        } else {
            throw new NotFoundHttpException();

        }
    }

    /**
     * @Route("/client", name="app_create_client")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function createClientAction(
        ClientRepository $clientRepository,
        ClientHandler $clientHandler,
        ObjectManager $manager,
        Request $request
    )
    {
        $data['email'] = $request->request->get('email');
        $data['passport'] = $request->request->get('passport');
        $data['password'] = $request->request->get('password');
        $data['vkId'] = $request->request->get('vkId');
        $data['faceBookId'] = $request->request->get('faceBookId');
        $data['googleId'] = $request->request->get('googleId');

        if(empty($data['email']) || empty($data['passport']) || empty($data['password'])) {
            return new JsonResponse(['error' => 'Недостаточно данных. Вы передали: '.var_export($data,1)],406);
        }

        if ($clientRepository->findOneByPassportOrEmail($data['passport'], $data['email'])) {
            return new JsonResponse(['error' => 'Клиент уже существует'],406);
        }

        $client = $clientHandler->createNewClient($data);
        $manager->persist($client);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }

    /**
     * @Route("/client/email/{email}", name="app_sign-in")
     * @Method("GET")
     * @param ClientRepository $clientRepository
     * @param string $email
     * @return JsonResponse
     */
    public function signInClientAction(ClientRepository $clientRepository, string $email)
    {
        /** @var Client $result */
        $result = $clientRepository->findOneByEmail($email);
        if ($result) {
            return new JsonResponse($result->toArray());
        } else {
            throw new NotFoundHttpException();
        }
    }
}