<?php

namespace App\Controller;

use App\Entity\BookedObject;
use App\Entity\Cottage;
use App\Entity\Pension;
use App\Model\Object\ObjectHandler;
use App\Repository\BookedObjectRepository;
use App\Repository\BookingObjectRepository;
use App\Repository\ClientRepository;
use App\Repository\CottageRepository;
use App\Repository\PensionRepository;
use DateInterval;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class objectController extends Controller
{
    /**
     * @Route("/object/new/create", name="app_create_object")
     * @Method("POST")
     * @param ObjectManager $manager
     * @param Request $request
     * @param ObjectHandler $objectHandler
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function createObjectAction(
        ObjectManager $manager,
        Request $request,
        ObjectHandler $objectHandler,
        ClientRepository $clientRepository
    )
    {
        $data['discriminator'] = $request->request->get('discriminator');
        $data['name'] = $request->request->get('name');
        $data['amountRooms'] = $request->request->get('amountRooms');
        $data['contactPerson'] = $request->request->get('contactPerson');
        $data['contactTelephone'] = $request->request->get('contactTelephone');
        $data['address'] = $request->request->get('address');
        $data['pricePerNight'] = $request->request->get('pricePerNight');
        $landlordEmail = $request->request->get('landlord');
        $data['landlord'] = $clientRepository->findOneByEmail($landlordEmail);

        if ($data['discriminator'] == $objectHandler::COTTAGE){
            $data['kitchen'] = $request->request->get('kitchen');
            $data['garden'] = $request->request->get('garden');
        }
        elseif ($data['discriminator'] == $objectHandler::PENSION){
            $data['disco'] = $request->request->get('disco');
            $data['cinema'] = $request->request->get('cinema');
        }
        $object = $objectHandler->createObject($data);
        $manager->persist($object);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }

    /**
     * @Route("/object/get/objects", name="app_get_objects")
     * @Method("GET")
     * @param BookingObjectRepository $bookingObjectRepository
     * @return JsonResponse
     */
    public function getAllObjects(BookingObjectRepository $bookingObjectRepository)
    {
        $objectsArray = [];
        $objects = $bookingObjectRepository->findAll();
        /** @var Cottage | Pension $object */
        if ($objects) {
            foreach ($objects as $object){
                $array = $object->toArray();
                $array['class'] = $entityName = get_class($object);
                $objectsArray[] = $array;
            }
            return new JsonResponse($objectsArray);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/object/get/filtration", name="app_get_filtration")
     * @Method("GET")
     * @param BookingObjectRepository $bookingObjectRepository
     * @param Request $request
     * @param PensionRepository $pensionRepository
     * @param CottageRepository $cottageRepository
     * @return JsonResponse
     */
    public function getFiltrationObjectsAction(
        BookingObjectRepository $bookingObjectRepository,
        Request $request,
        PensionRepository $pensionRepository,
        CottageRepository $cottageRepository
    )
    {
        $discriminator = $request->query->get('discriminator');
        $from = $request->query->get('from');
        $before = $request->query->get('before');
        $word = $request->query->get('word');

        if ($discriminator){
            if ($discriminator == get_class(new Pension())){
                $objects = $pensionRepository->findByFiltration($word, $from, $before);
            } else {
                $objects = $cottageRepository->findByFiltration($word, $from, $before);
            }
        } else {
            $objects = $bookingObjectRepository->findByFiltration($word, $from, $before);
        }

        $objectsArray = [];

        /** @var Cottage | Pension $object */
        if ($objects) {
            foreach ($objects as $object){
                $array = $object->toArray();
                $array['class'] = $entityName = get_class($object);
                $objectsArray[] = $array;
            }
            return new JsonResponse($objectsArray);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/get/concrete/object", name="app_get_concrete_object")
     * @Method("GET")
     * @param BookingObjectRepository $bookingObjectRepository
     * @param Request $request
     * @param BookedObjectRepository $bookedObjectRepository
     * @return JsonResponse
     */
    public function getOneObjectAction(
        BookingObjectRepository $bookingObjectRepository,
        Request $request,
        BookedObjectRepository $bookedObjectRepository
    )
    {
        $nameObject = $request->query->get('name');
        /** @var Cottage | Pension $object */
        $object = $bookingObjectRepository->findOneByNameObject($nameObject);
        if ($object) {
            $object_id = $object->getId();
            $booking = $bookedObjectRepository->getBookedRooms($object_id);

            $objectArray = $object->toArray();
            $objectArray['class'] = get_class($object);
            $objectArray['booking'] = $booking;

            return new JsonResponse($objectArray);
        } else {
            throw new NotFoundHttpException();

        }
    }
    /**
     * @Route("/booking/object/room", name="app_create_booking_room")
     * @Method("POST")
     * @param BookingObjectRepository $bookingObjectRepository
     * @param Request $request
     * @param ObjectManager $manager
     * @param ClientRepository $clientRepository
     * @param BookedObjectRepository $bookedObjectRepository
     * @return JsonResponse
     * @throws \Exception
     */
    public function bookingObjectAction(
        BookingObjectRepository $bookingObjectRepository,
        Request $request,
        ObjectManager $manager,
        ClientRepository $clientRepository,
        BookedObjectRepository $bookedObjectRepository
    )
    {
        $nameObject = $request->request->get('name');
        $numberRoom = $request->request->get('room');
        $clientEmail = $request->request->get('tenant');
        $client = $clientRepository->findOneByEmail($clientEmail);

        /** @var Cottage | Pension $object */
        $object = $bookingObjectRepository->findOneByNameObject($nameObject);
        $booking = new BookedObject();

        /** @var BookedObject $dqlResult */
        $dqlResult = $bookedObjectRepository->findByBookingDate($object, $numberRoom, $client);
        if ($dqlResult){
            $date = $dqlResult->getBookingDateEnding()->add(new DateInterval('P7D'));
        } else {
            $newDate = new \DateTime();
            $date = $newDate->add(new DateInterval('P7D'));
        }
        $booking
                ->setObject($object)
                ->setRoom($numberRoom)
                ->setBookingDateEnding($date)
                ->setTenant($client)
            ;
        $manager->persist($booking);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }
}
