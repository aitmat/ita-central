<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/13/18
 * Time: 7:39 PM
 */

namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\Cottage;
use App\Entity\Pension;
use App\Entity\User;
use App\Model\Client\ClientHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class BookingFixtures extends Fixture
{

    /**
     * @var ClientHandler
     */
    private $clientHandler;

    public function __construct(ClientHandler $clientHandler)
    {
        $this->clientHandler = $clientHandler;
    }

    public function load(ObjectManager $manager)
    {
        $bookingObject1 = new Cottage();
        $bookingObject1
            ->setName('Радуга')
            ->setAmountRooms(2)
            ->setContactPerson('Иванов Альберт')
            ->setContactTelephone('+996 770 777 555')
            ->setAddress('42.651132, 77.195466')
            ->setPricePerNight('1000')
            ->setKitchen(true)
            ->setGarden(true);
        $manager->persist($bookingObject1);


        $bookingObject2 = new Cottage();
        $bookingObject2
            ->setName('Семья')
            ->setAmountRooms(2)
            ->setContactPerson('Хозяин')
            ->setContactTelephone('+996 770 222 555')
            ->setAddress('42.645513, 77.186680')
            ->setPricePerNight('500')
            ->setKitchen(true)
            ->setGarden(true);
        $manager->persist($bookingObject2);

        $bookingObject3 = new Cottage();
        $bookingObject3
            ->setName('Жаннат')
            ->setAmountRooms(10)
            ->setContactPerson('Админ')
            ->setContactTelephone('+996 770 777 555')
            ->setAddress('42.642437, 77.080866')
            ->setPricePerNight('1500')
            ->setKitchen(true)
            ->setGarden(true);
        $manager->persist($bookingObject3);


        $bookingObject4 = new Cottage();
        $bookingObject4
            ->setName('Большой коттедж')
            ->setAmountRooms(6)
            ->setContactPerson('Иван')
            ->setContactTelephone('+996 770 222 555')
            ->setAddress('42.599994, 76.914698')
            ->setPricePerNight('3000')
            ->setKitchen(false)
            ->setGarden(true);
        $manager->persist($bookingObject4);

        $pension1 = new Pension();
        $pension1
            ->setName('Лагуна Сити')
            ->setAmountRooms(2)
            ->setContactPerson('Елена')
            ->setContactTelephone('+996 770 222 555')
            ->setAddress('42.685763, 77.341572')
            ->setPricePerNight('4000')
            ->setCinema(true)
            ->setDisco(true);
        $manager->persist($pension1);

        $pension2 = new Pension();
        $pension2
            ->setName('Голубой Иссык-Куль')
            ->setAmountRooms(15)
            ->setContactPerson('Григорий')
            ->setContactTelephone('+996 770 222 555')
            ->setAddress('42.588874, 76.722437')
            ->setPricePerNight('2000')
            ->setCinema(true)
            ->setDisco(true);
        $manager->persist($pension2);

        $pension3 = new Pension();
        $pension3
            ->setName('Золотые пески')
            ->setAmountRooms(7)
            ->setContactPerson('Елена')
            ->setContactTelephone('+996 770 222 555')
            ->setAddress('42.647881, 77.186883')
            ->setPricePerNight('1500')
            ->setCinema(true)
            ->setDisco(true);
        $manager->persist($pension3);

        $pension4 = new Pension();
        $pension4
            ->setName('Пансионат Максат')
            ->setAmountRooms(10)
            ->setContactPerson('Елена')
            ->setContactTelephone('+996 770 222 555')
            ->setAddress('42.560558, 76.649652')
            ->setPricePerNight('1000')
            ->setCinema(true)
            ->setDisco(true);
        $manager->persist($pension4);

        $pension5 = new Pension();
        $pension5
            ->setName('Тулпар')
            ->setAmountRooms(5)
            ->setContactPerson('Елена')
            ->setContactTelephone('+996 770 222 555')
            ->setAddress('42.275924, 77.782908')
            ->setPricePerNight('500')
            ->setCinema(true)
            ->setDisco(true);
        $manager->persist($pension5);


        $manager->flush();
    }
}
