<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/13/18
 * Time: 7:39 PM
 */

namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\User;
use App\Model\Client\ClientHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ClientFixtures extends Fixture
{

    /**
     * @var ClientHandler
     */
    private $clientHandler;

    public function __construct(ClientHandler $clientHandler)
    {
        $this->clientHandler = $clientHandler;
    }

    public function load(ObjectManager $manager)
    {
        $client = $this->clientHandler->createNewClient([
            'email' => '123@123.ru',
            'passport' => 'some & passport',
            'password' => 'pass123',
        ]);
        $manager->persist($client);

        $client = $this->clientHandler->createNewClient([
            'email' => '111@111.ru',
            'passport' => 'passport & some',
            'password' => 'pass111',
        ]);

        $manager->persist($client);
        $manager->flush();
    }
}
