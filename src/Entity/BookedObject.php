<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookedObjectRepository")
 */
class BookedObject
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BookingObject", inversedBy="booking")
     */
    private $object;

    /**
     * @ORM\Column(type="integer", length=124)
     */
    private $room;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $bookingDateEnding;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="booking")
     */
    private $tenant;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param mixed $object
     * @return BookedObject
     */
    public function setObject($object)
    {
        $this->object = $object;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param mixed $room
     * @return BookedObject
     */
    public function setRoom($room)
    {
        $this->room = $room;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @param mixed $bookingDateEnding
     * @return BookedObject
     */
    public function setBookingDateEnding($bookingDateEnding)
    {
        $this->bookingDateEnding = $bookingDateEnding;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBookingDateEnding()
    {
        return $this->bookingDateEnding;
    }

    /**
     * @param mixed $tenant
     * @return BookedObject
     */
    public function setTenant($tenant)
    {
        $this->tenant = $tenant;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTenant()
    {
        return $this->tenant;
    }
}
