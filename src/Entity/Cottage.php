<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CottageRepository")
 */
class Cottage extends BookingObject
{
    /**
     * @ORM\Column(type="boolean")
     */
    private $kitchen;

    /**
     * @ORM\Column(type="boolean")
     */
    private $garden;

    /**
     * @param mixed $kitchen
     * @return Cottage
     */
    public function setKitchen($kitchen)
    {
        $this->kitchen = $kitchen;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKitchen()
    {
        return $this->kitchen;
    }

    /**
     * @param mixed $garden
     * @return Cottage
     */
    public function setGarden($garden)
    {
        $this->garden = $garden;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGarden()
    {
        return $this->garden;
    }

    public function toArray(){
        return [
            'name' => $this->getName(),
            'amountRooms' => $this->getAmountRooms(),
            'contactPerson' => $this->getContactPerson(),
            'contactTelephone' => $this->getContactTelephone(),
            'address' => $this->getAddress(),
            'pricePerNight' => $this->getPricePerNight(),
            'kitchen' => $this->kitchen,
            'garden' => $this->garden,
            'landlord' => $this->getLandlord(),
        ];
    }
}
