<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingObjectRepository")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discriminator", type="string")
 * @DiscriminatorMap({"BookingObject" = "BookingObject", "Cottage" = "Cottage", "Pension" = "Pension"})
 */
class BookingObject
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=124)
     */
    private $name;

    /**
     * @ORM\Column(type="integer", length=16)
     */
    private $amountRooms;

    /**
     * @ORM\Column(type="string", length=254)
     */
    private $contactPerson;

    /**
     * @ORM\Column(type="string", length=254)
     */
    private $contactTelephone;

    /**
     * @ORM\Column(type="string", length=254)
     */
    private $address;

    /**
     * @ORM\Column(type="decimal", length=10, scale=2)
     */
    private $pricePerNight;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="object")
     */
    private $landlord;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BookedObject", mappedBy="object")
     */
    private $booking;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param mixed $name
     * @return BookingObject
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $amountRooms
     * @return BookingObject
     */
    public function setAmountRooms($amountRooms)
    {
        $this->amountRooms = $amountRooms;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmountRooms()
    {
        return $this->amountRooms;
    }

    /**
     * @param mixed $contactPerson
     * @return BookingObject
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * @param mixed $contactTelephone
     * @return BookingObject
     */
    public function setContactTelephone($contactTelephone)
    {
        $this->contactTelephone = $contactTelephone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContactTelephone()
    {
        return $this->contactTelephone;
    }

    /**
     * @param mixed $address
     * @return BookingObject
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $pricePerNight
     * @return BookingObject
     */
    public function setPricePerNight($pricePerNight)
    {
        $this->pricePerNight = $pricePerNight;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPricePerNight()
    {
        return $this->pricePerNight;
    }

    /**
     * @param mixed $landlord
     * @return BookingObject
     */
    public function setLandlord($landlord)
    {
        $this->landlord = $landlord;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLandlord()
    {
        return $this->landlord;
    }

    /**
     * @param mixed $booking
     * @return BookingObject
     */
    public function setBooking($booking)
    {
        $this->booking = $booking;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBooking()
    {
        return $this->booking;
    }
}
