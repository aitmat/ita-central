<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PensionRepository")
 */
class Pension extends BookingObject
{
    /**
     * @ORM\Column(type="boolean")
     */
    private $cinema;

    /**
     * @ORM\Column(type="boolean")
     */
    private $disco;

    /**
     * @param mixed $cinema
     * @return Pension
     */
    public function setCinema($cinema)
    {
        $this->cinema = $cinema;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCinema()
    {
        return $this->cinema;
    }

    /**
     * @param mixed $disco
     * @return Pension
     */
    public function setDisco($disco)
    {
        $this->disco = $disco;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDisco()
    {
        return $this->disco;
    }

    public function toArray(){
        return [
            'name' => $this->getName(),
            'amountRooms' => $this->getAmountRooms(),
            'contactPerson' => $this->getContactPerson(),
            'contactTelephone' => $this->getContactTelephone(),
            'address' => $this->getAddress(),
            'pricePerNight' => $this->getPricePerNight(),
            'kitchen' => $this->disco,
            'garden' => $this->cinema,
            'landlord' => $this->getLandlord(),
        ];
    }
}
