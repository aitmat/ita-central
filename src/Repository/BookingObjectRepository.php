<?php

namespace App\Repository;

use App\Entity\BookingObject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BookingObject|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookingObject|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookingObject[]    findAll()
 * @method BookingObject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingObjectRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BookingObject::class);
    }

    public function findOneByPassportOrEmail(string $passport, string $email) : ?BookingObject
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.passport = :passport')
                ->orWhere('a.email = :email')
                ->setParameter('passport', $passport)
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findByFiltration($word, $from, $before)
    {
        return $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.name LIKE :word')
            ->andWhere('a.pricePerNight between  :from and :before')
            ->setParameter('word', '%'.$word.'%')
            ->setParameter('from',  $from ?? 500)
            ->setParameter('before',  $before ?? 100000)
            ->getQuery()
            ->getResult();
    }

    public function findOneByNameObject($nameObject)
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.name = :name')
                ->setParameter('name', $nameObject)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}