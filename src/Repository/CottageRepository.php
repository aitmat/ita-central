<?php

namespace App\Repository;

use App\Entity\Cottage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Cottage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cottage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cottage[]    findAll()
 * @method Cottage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CottageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Cottage::class);
    }

    public function findByFiltration($word, $from, $before)
    {
        return $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.name LIKE :word')
            ->andWhere('a.pricePerNight between  :from and :before')
            ->setParameter('word', '%'.$word.'%')
            ->setParameter('from',  $from ?? 500)
            ->setParameter('before',  $before ?? 100000)
            ->getQuery()
            ->getResult();
    }
}
