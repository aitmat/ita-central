<?php

namespace App\Repository;

use App\Entity\BookedObject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BookedObject|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookedObject|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookedObject[]    findAll()
 * @method BookedObject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookedObjectRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BookedObject::class);
    }

    public function findByBookingDate($object, $numberRoom, $client)
    {
        try {
            return $this->createQueryBuilder('a')
                ->where('a.object = :object')
                ->andWhere('a.room = :numberRoom')
                ->andWhere('a.tenant = :tenant')
                ->orderBy('a.bookingDateEnding', 'DESC')
                ->setParameter('object', $object)
                ->setParameter('numberRoom', $numberRoom)
                ->setParameter('tenant', $client)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param $object_id
     * @return mixed
     */
    public function getBookedRooms($object_id)
    {
        return $this->createQueryBuilder('a')
            ->select('a.room, MAX(a.bookingDateEnding)')
            ->where('a.object = :object_id')
            ->setParameter('object_id', $object_id)
            ->groupBy('a.room')
            ->getQuery()
            ->getResult();
    }
}
