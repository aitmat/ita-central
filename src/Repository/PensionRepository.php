<?php

namespace App\Repository;

use App\Entity\Pension;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Pension|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pension|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pension[]    findAll()
 * @method Pension[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PensionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Pension::class);
    }

    public function findByFiltration($word, $from, $before)
    {
        return $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.name LIKE :word')
            ->andWhere('a.pricePerNight between  :from and :before')
            ->setParameter('word', '%'.$word.'%')
            ->setParameter('from',  $from ?? 500)
            ->setParameter('before',  $before ?? 100000)
            ->getQuery()
            ->getResult();
    }
}
